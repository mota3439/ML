function EEG = std_ica(EEG, HPF, LPF, THRES)
    %
    % std_ica.m
    %
    % needs 2-D EEG dataset and
    % high-pass filter (typically 1Hz)
    % low-pass filter  (40Hz ?)
    % Rejection threshold (in SD, between 2 and 5) 

    if nargin < 1
        help std_ica;
    elseif length(size(EEG.data))==3
        error('EEG.data must be continuous, not epoched!');
    end


    MEEG = pop_eegfiltnew(EEG, HPF,LPF,[],0,[],0);
    MEEG = eeg_regepochs(MEEG);
    MEEG = pop_jointprob(MEEG, 1, [1:MEEG.nbchan] ,THRES,THRES,0,1);
    MEEG = pop_rejkurt(  MEEG, 1, [1:MEEG.nbchan] ,THRES,THRES,0,1);
    
    % quick, do not use, only for development purposes
    MEEG = pop_runica(MEEG, 'pca',15,'interupt','on'); 
    % use this one later
    %MEEG = pop_runica(MEEG, 'extended',1,'interupt','on');
    
    EEG.icawinv = MEEG.icawinv;
    EEG.icaweights = MEEG.icaweights;
    EEG.icasphere = MEEG.icasphere;
    EEG.icawinv = MEEG.icawinv;
    EEG.icachansind = MEEG.icachansind;
    EEG = eeg_checkset(EEG);

end

