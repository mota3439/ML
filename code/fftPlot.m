function fftPlot(open_spec, closed_spec)
    figure;
    F = open_spec.SPEC_F;
    P = squeeze(mean(open_spec.EVENT_P,1));
    plot(F,10*log10(P), 'b');
    hold on;
    plot(F,10*log10(mean(P,2)), 'b', 'Linew', 2);
    P = squeeze(mean(closed_spec.EVENT_P,1));
    plot(F,10*log10(P), 'r');
    plot(F,10*log10(mean(P,2)), 'r', 'Linew', 2);
    ylabel('Power (dB)');
    xlabel('Frequency (Hz)');
    grid
    axis tight;
    title('Power spectra eyes open (b) vs eyes closed (r)')
end