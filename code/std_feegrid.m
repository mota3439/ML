% std_feegrid.m
%
%
PATHIN = '/Users/std/Dropbox/BMBF_NeuroCommTrainer/fEEGrid data/';
cd(PATHIN);

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
EEG = pop_loadbv(PATHIN, 'sab0001.vhdr', [], []);
EEG = pop_chanedit(EEG,'load',{'feegrid.locs' 'filetype' 'autodetect'});
eeg_eventtypes(EEG);
EEG.event(4) =[]; % del labelling error event
EEG = eeg_checkset(EEG);
EEG = std_ica(EEG,2,40,2);
[ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);

eeglab redraw

% check
EEG = pop_subcomp(EEG, [1 2], 0);
EEG = pop_reref( EEG, []);


%   END                       15
%   EYES OPEN                 5
%   EYES CLOSED               5
%   boundary                  1
%   VERTICAL EYE MOVEMENTS    1
%   TALKING                   1
%   LATERAL EYE MOVEMENTS     1
%   HEAD ROTATION             1
%   BLINK                     1

RES =[];
[types,numbers] = eeg_eventtypes(EEG);


% FFT OPEN
EP = numbers(strcmpi('EYES OPEN', types))

idx=1
while idx<EP
    for e = 1:length(EEG.event)
        if isequal(EEG.event(e).type,'EYES OPEN')
            data = [];
            data = EEG.data(:,EEG.event(e).latency:EEG.event(e+1).latency);
            size(data)
        end
    end
    idx = idx+1;
    [Pxx,F] = pwelch(data',8192,4096,[],EEG.srate,'onesided');
    Pxx = Pxx(1:500,:);
    F = F(1:500);
    RES.EYES_OPEN_P(EP,:,:) = Pxx;
    RES.SPEC_F = F;
end


% FFT CLOSED
EP = numbers(strcmpi('EYES CLOSED', types))

idx=1
while idx<EP
    for e = 1:length(EEG.event)
        if isequal(EEG.event(e).type,'EYES CLOSED')
            data = [];
            data = EEG.data(:,EEG.event(e).latency:EEG.event(e+1).latency);
            %size(data);
        end
    end
    idx = idx+1;
    [Pxx,F] = pwelch(data',8192,4096,[],EEG.srate,'onesided');
    Pxx = Pxx(1:500,:);
    F = F(1:500);
    RES.EYES_CLOSED_P(EP,:,:) = Pxx;
end

figure;
F = RES.SPEC_F;
P = squeeze(mean(RES.EYES_OPEN_P,1));
plot(F,10*log10(P), 'b');
hold on;
plot(F,10*log10(mean(P,2)), 'b', 'Linew', 2);
P = squeeze(mean(RES.EYES_CLOSED_P,1));
plot(F,10*log10(P), 'r');
plot(F,10*log10(mean(P,2)), 'r', 'Linew', 2);
ylabel('Power (dB)');
xlabel('Frequency (Hz)');
grid
axis tight;
title('Power spectra eyes open (b) vs eyes closed (r)')

figure;
eegplot(EEG.data, 'limits', [1040 1060], 'data2', ALLEEG(2).data)



