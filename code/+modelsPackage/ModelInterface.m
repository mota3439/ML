%% Abstract class implementation which defines the behaviour of all model instances we want to use. 
%
%   Each subclass will have a feature object containing the preprocessed data and the
%   extracted features and two methods train() and test()
%
% Author: Sarah Blum, 2017
classdef ModelInterface < handle
   
    properties (Abstract)
        featureObj
    end
    
    methods (Abstract)
       train(featureObj);
       test(featObj);
    end
end