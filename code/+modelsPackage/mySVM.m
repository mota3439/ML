%% This class inherits from the abstract class ModelInterface.
% This means that mySVM inherits the properties and functionality provided by the 
% superclass ModelInterface. Every object created from the class 
classdef mySVM < modelsPackage.ModelInterface
    
    % add properties to the abstract model class which are specific to this type of model
    properties
        featureObj
        penalty
        loss
        kernel
        model
        predict_label
        score
        prob_values
    end
    
    methods (Static)
        function loadLib()
           % svm (tutorial from: https://sites.google.com/site/kittipat/libsvm_matlab)
            addpath('../libsvm-3.22/matlab');
        end
    end
    
    % implement the abstract methods from the interface by providing specific
    % functionality which is applicable for this model
    methods
        
        % contructor
        function self = mySVM(features)
            self.featureObj = features;
        end
        
        function train(self)
            X = self.featureObj.trainSet(:,2);
            LABELS = self.featureObj.trainSet(:,1);
            self.model = fitcsvm(X, LABELS); %, '-c 1 -g 0.07 -b 1');
        end
            
        % Use the SVM model to classify the data
        function test(self)
            X = double(self.featureObj.testSet(:,2));
            [self.predict_label, self.score] = predict(self.model, X); %, '-b 1');
        end
        
    end
    
    
    
    
    
end