%% IMPORT
% import headgrid data. Assume we are in /code and that rawdata lies in /data
p = '../data';
fname = 'sab0001.vhdr';
chanloc = '../feegrid.locs';

% create data Object which holds the preprocessed data for further processing
d = DataObj;
d.p = p;
d.fname = fname;
d.chanlocFile = chanloc;

% import data and fill them into field data
d.import_with_eeglab();

%% PREPROCESS
HPF = 1;
LPF = 40;
THRES = 3;
d.ICA(HPF, LPF, THRES);
%TODO store processed data in /output
%(https://de.mathworks.com/help/matlab/ref/saveobj.html)


%% FEATURE EXTRACTION
% create a feature object which contains the data that we processed earlier
f = FeaturesObj(d);

% extract spectral power for two conditions (examplary)
open_spec = f.eventFFT('EYES OPEN');
closed_spec = f.eventFFT('EYES CLOSED');

% extract alpha power at all channels for each trial
[alpha_closed, alpha_open] = extractAlphaBadly(open_spec, closed_spec, 10, 10.5);

%% Compose Datasets 
f.composeDatasets(alpha_open, alpha_closed);
% how many entries from the feature set do we want to use for training
ntrain = 2;
f.splitSets(ntrain); 

%% CLASSIFY. libsvm wants data as array, not as a table!
tinySVM = modelsPackage.mySVM(f);
tinySVM.train();
tinySVM.test(); 

%% Compatibility with other classifiers
% just use table(f.D), table(f.trainSet) and so on