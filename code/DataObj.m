% This class models the input data, for example recorded with the headgrid. 
% The data objects can be passed to the feature extraction and machine learning pipeline.
%
% Author: Sarah Blum, 2017
classdef DataObj < handle
   
   properties 
        p           % path to rawdata
        fname       % data
        chanlocFile
        data        % this contains the eeglab structure/ the data
   end
   
   
   methods
       % Import function for eeglab data. Expects data in vhdr format (and therefore requires
       % the biosemi plugin). 
       % Uses the path, chanloc, filename field from the object
       % TODO: detect file format and chose import accordingly 
       % TODO: return figure handle if we want to use the GUI after the import
       function import_with_eeglab(self)
            [~, EEG , ~, ~] = eeglab;
            EEG = pop_loadbv(self.p, self.fname, [], []);
            EEG = pop_chanedit(EEG,'load',{self.chanlocFile, 'filetype' 'autodetect'});
            % show event summary
            eeg_eventtypes(EEG);
            % del labelling error event
            EEG.event(4) =[];
            EEG = eeg_checkset(EEG);
            % is this good?
            self.data = EEG;
       end
    
       function artefactDetection(self)  
       end
       
       function filter(self)
       end
       
       function epochData(eventnames, interval)
           self.data = pop_epoch(self.data, {eventnames}, interval);
       end
       
       % small test function, don't use as is
       function ICA(self, HPF, LPF, THRES)
           self.data = std_ica(self.data, HPF, LPF, THRES);
       end
       
       
       
       
       
   end
    
    
end