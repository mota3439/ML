% This class contains all the methods needed for feature extraction from the Dataobj object. 
% Extracted features are stored in the fields for further use in a classifier.
%
% Author: Sarah Blum, 2017
% Question to self: do we need another class for features of different modalities than EEG? 
classdef FeaturesObj < handle
    % list of properties taken from O'Regan (2012)
    properties
        dataObj
        %time domain
        curveLength 
        zeroCrossing
        
        %frequency domain
        peakFreq
        powerSpect
        
        %entropy-based features
        svdEntr
        shannonEntr
        
        % datasets for the model instance
        D
        testSet
        trainSet
    end
    
    methods
        % constructor: create feature objects with dataObj 
        function self = FeaturesObj(dataObj)
           self.dataObj = dataObj; 
        end
        
        % perform FFT for data from a spe
        function RES = eventFFT(self, eventname)
            [types, numbers] = eeg_eventtypes(self.dataObj.data);
            EP = numbers(strcmpi(eventname, types));

            idx = 1;
            while idx <= EP
                for e = 1:length(self.dataObj.data.event)
                    if isequal(self.dataObj.data.event(e).type,eventname)
                        % from one event to the next 
                        dat = self.dataObj.data.data(:,self.dataObj.data.event(e).latency : ...
                                                       self.dataObj.data.event(e+1).latency);
                    
                        % wth
                        [Pxx,F] = pwelch(dat',8192,4096,[],self.dataObj.data.srate,'onesided');
                        % take only the first 500 frequency entries
                        Pxx = Pxx(1:500,:);
                        F = F(1:500);
                        %TODO do we need to store results from different events in the same 
                        % feature object?? for now, just return the result

                        % trialcount x frequpower x channel
                        RES.EVENT_P(idx,:,:) = Pxx;
                        % frequencies
                        RES.SPEC_F = F;
                        idx = idx+1;
                    end
                    
                end
            end 
        end
        
        % TODO this can be private
        % compose dataset for variable number of features.
        function composeDatasets(self, varargin)
            % celldisp(varargin)
            % varargin now contains all the feature vectors starting at index 1.. (and 'self')
            nfeat= nargin-1;  
            % fill in the features. they will get class labels 1 to nfeat 
            for i = 1: nfeat
                dat{i} = [repmat(i,1,length(varargin{i}))', varargin{i}'];
            end
            % combine to a matrix which contains all labels and features 
            self.D = cat(1,dat{:});
        end
        
        function splitSets(self, ntrain)
            % how many feature classes do we have
            nfeat = max(self.D(:,1));
            % get the indices for all feature classes
            for i = 1: nfeat
                trainidx{i} = find(self.D == i);
                % take the first ntrain entries from each feature class for the training
                trainS{i} = self.D(trainidx{i}(1:ntrain), :);
                % take the remaining entries for the test set
                testS{i}  = self.D(trainidx{i}(ntrain+1 : end), :);
            end
            self.trainSet = cat(1, trainS{:});
            self.testSet  = cat(1,testS{:});
        end
        
        
    end
end