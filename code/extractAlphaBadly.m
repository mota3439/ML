function [alpha_closed, alpha_open] = extractAlphaBadly(open_spec, closed_spec, hz1, hz2)

    idx = open_spec.SPEC_F > hz1 & open_spec.SPEC_F <hz2;

    % exctract the "alpha power" for each trial over all channels 
    for t = 1:5
        alpha_closed(t) = max(mean(closed_spec.EVENT_P(t,idx==1,:),3));
        alpha_open(t) = max(mean(open_spec.EVENT_P(t,idx==1,:),3));
    end

end